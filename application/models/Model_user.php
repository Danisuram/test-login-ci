<?php

class Model_user extends CI_Model
{
    function getUserdata()
    {
        return $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    }
}
