<?php

class Model_auth extends CI_Model
{
    function getUser($email)
    {
        return $this->db->get_where('user', ['email' => $email])->row_array();
    }

    function getToken($token)
    {
        return $this->db->get_where('user_token', ['token' => $token])->row_array();
    }

    function addUser($upload_image)
    {
        $email = $this->input->post('email', true);
        $data = [
            'name'          => htmlspecialchars($this->input->post('name')),
            'email'         => htmlspecialchars($email),
            'image'         => $upload_image,
            'password'      => password_hash(
                $this->input->post('password1'),
                PASSWORD_DEFAULT
            ),
            'is_active'    => 0,
            'date_created' => time()
        ];

        $this->db->insert('user', $data);
    }

    function addToken($token)
    {
        $email = $this->input->post('email', true);
        $user_token = [
            'email' => $email,
            'token' => $token,
            'date_created' => time()
        ];

        $this->db->insert('user_token', $user_token);
    }

    function updateActivation($email)
    {
        $this->db->set('is_active', 1);
        $this->db->where('email', $email);
        $this->db->update('user');

        $this->db->delete('user_token', ['email' => $email]);
    }

    function updatePassword()
    {
        $password = password_hash(
            $this->input->post('password1'),
            PASSWORD_DEFAULT
        );
        $email = $this->session->userdata('reset_email');

        $this->db->set('password', $password);
        $this->db->where('email', $email);
        $this->db->update('user');
    }
}
