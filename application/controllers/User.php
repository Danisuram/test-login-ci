<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_user');
    }

    public function index()
    {
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }

        $data['title'] = 'User Page';
        $data['user'] = $this->Model_user->getUserdata();

        $this->load->view('user/index', $data);
    }
}
