<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('Model_auth');
    }
    public function index()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login Page';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->Model_auth->getUser($email);
        if ($user) {
            if ($user['is_active'] == 1) {
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'email' => $user['email']
                    ];

                    $this->session->set_userdata($data);
                    redirect('user');
                } else {
                    $this->session->set_flashdata('message', '
                    <div class="alert alert-danger" role="alert">
                        Wrong password!
                    </div>
                    ');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '
                <div class="alert alert-danger" role="alert">
                    This Email has not been activated
                </div>
                ');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '
            <div class="alert alert-danger" role="alert">
                Email is not registered
            </div>
            ');
            redirect('auth');
        }
    }

    public function registration()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'This email has already registered!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');


        if ($this->form_validation->run() == false) {
            $data['title'] = 'User Registration';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');
        } else {

            $upload_image = $this->uploadImage();

            $token = base64_encode(random_bytes(32));

            $this->Model_auth->addUser($upload_image);
            $this->Model_auth->addToken($token);

            $this->_sendEmail($token, 'verify');
            $this->session->set_flashdata('message', '
            <div class="alert alert-success" role="alert">
                Congratulation your account has been created. Please activated your account
            </div>
            ');
            redirect('auth');
        }
    }

    function uploadImage()
    {
        $upload_image = $_FILES['image']['name'];

        if ($upload_image) {
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['upload_path']  = './assets/img/profile/';
            $this->load->library('upload');
            $this->upload->initialize($config);
            $this->upload->do_upload('image');
        }

        return $upload_image;
    }


    private function _sendEmail($token, $type)
    {
        $config = [
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_user' => 'dasura00@gmail.com',
            'smtp_pass' => 'wyhfudrirsyyxbze',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'newline'   => "\r\n"
        ];

        $this->load->library('email', $config);
        $this->email->initialize($config);

        $this->email->from('dasura00@gmail.com', 'Admin');
        $this->email->to($this->input->post('email'));

        if ($type == 'verify') {
            $this->email->subject('Account Verification');
            $this->email->message('Click this link to verify your account: <a 
            href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Activate</a>');
        } else if ($type == 'forgot') {
            $this->email->subject('Reset Password');
            $this->email->message('Click this link to reset your password: <a 
            href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Reset Password</a>');
        }

        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            die;
        }
    }

    public function verify()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->Model_auth->getUser($email);

        if ($user) {
            $user_token = $this->Model_auth->getToken($token);

            if ($user_token) {

                $this->Model_auth->updateActivation($email);

                $this->session->set_flashdata('message', '
                <div class="alert alert-success" role="alert">'
                    . $email
                    . ' has been activated! Please login 
                </div>
                ');
                redirect('auth');
            } else {
                $this->session->set_flashdata('message', '
                <div class="alert alert-danger" role="alert">
                    Invalid token
                </div>
                ');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '
            <div class="alert alert-danger" role="alert">
               Account activation failed! Wrong email
            </div>
            ');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->set_flashdata('message', '
            <div class="alert alert-success" role="alert">
               You have been logout
            </div>
            ');
        redirect('auth');
    }

    public function forgotPassword()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Forgot Password';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/forgot-password');
            $this->load->view('templates/auth_footer');
        } else {
            $email = $this->input->post('email');
            $user = $this->Model_auth->getUser($email);

            if ($user) {
                $token = base64_encode(random_bytes(32));
                $this->Model_auth->addToken($token);

                $this->_sendEmail($token, 'forgot');

                $this->session->unset_userdata('email');
                $this->session->set_flashdata('message', '
                <div class="alert alert-success" role="alert">
                    Please check your email to reset your password!
                </div>
                ');
                redirect('auth/forgotPassword');
            } else {
                $this->session->set_flashdata('message', '
                <div class="alert alert-danger" role="alert">
                    Email is not registered or activated
                </div>
                ');
                redirect('auth/forgotPassword');
            }
        }
    }

    public function resetPassword()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->Model_auth->getUser($email);


        if ($user) {
            $user_token = $this->Model_auth->getToken($token);

            if ($user_token) {
                $this->session->set_userdata('reset_email', $email);
                $this->changePassword();
            } else {
                $this->session->set_flashdata('message', '
                <div class="alert alert-danger" role="alert">
                    Reset password failed! Wrong token
                </div>
                ');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '
                <div class="alert alert-danger" role="alert">
                    Reset password failed! Wrong email
                </div>
                ');
            redirect('auth');
        }
    }

    public function changePassword()
    {
        if (!$this->session->userdata('reset_email')) {
            redirect('auth');
        }


        $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[6]|matches[password2]');
        $this->form_validation->set_rules('password2', 'Repeat Password', 'trim|required|min_length[6]|matches[password1]');
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Change Password';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/change-password');
            $this->load->view('templates/auth_footer');
        } else {
            $this->Model_auth->updatePassword();

            $this->session->unset_userdata('reset_email');

            $this->session->set_flashdata('message', '
                <div class="alert alert-success" role="alert">
                    Password has been change! Please login.
                </div>
                ');
            redirect('auth');
        }
    }
}
